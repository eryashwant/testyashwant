//
//  MoviesList.swift
//  TestYashwant
//
//  Created by whf on 20/05/20.
//  Copyright © 2020 Yashwant. All rights reserved.
//

import Foundation


class MoviesList: Codable {
    var image: String
    var title: String
    var rating: Double
    var genre: [String]
    
    enum CodingKeys: String, CodingKey {
        case image
        case title
        case rating
        case genre
    }
}
