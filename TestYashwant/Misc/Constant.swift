//
//  AppDelegate.swift
//  TestYashwant
//
//  Created by whf on 20/05/20.
//  Copyright © 2020 Yashwant. All rights reserved.
//

import UIKit

let appDelegate = UIApplication.shared.delegate as! AppDelegate

enum Application {
    
    //Applicatipn keyWindow
    static var keyWindow: UIWindow {
        return UIApplication.shared.keyWindow!
    }
}
