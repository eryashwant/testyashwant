//
//  AppDelegate.swift
//  TestYashwant
//
//  Created by whf on 20/05/20.
//  Copyright © 2020 Yashwant. All rights reserved.
//

import Foundation

import Kingfisher

struct KingfisherHelper {
    
    static func configure(maxCacheSize: UInt, maxCachePeriod: TimeInterval) {
        ImageCache.default.diskStorage.config.sizeLimit = maxCacheSize
        ImageCache.default.diskStorage.config.expiration = .seconds(maxCachePeriod)
        
        KingfisherManager.shared.cache.diskStorage.config.pathExtension = "jpg"
    }
    
}

extension UIImageView {
    
    func setImage(with url: URL) {
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url)
    }
    
    func setImage(with url: URL, size: CGSize) {
        
        let processor = ResizingImageProcessor(referenceSize: size, mode: .aspectFill)
        
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url, options: [.processor(processor)])
    }
    
    func cancelDownload() {
        self.kf.cancelDownloadTask()
    }
    
    func setImageWithPlaceholder(with url: URL?, placeholderImage: UIImage) {
        self.kf.setImage(with: url, placeholder: placeholderImage)
    }
}
