//
//  MoviesListVM.swift
//  TestYashwant
//
//  Created by whf on 20/05/20.
//  Copyright © 2020 Yashwant. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class MoviesListVM {
  
    // MARK: - Instance Properties
    var moviesList = [MoviesList]()
    
    // MARK:- API
    func callListingApi(_ handler: @escaping (() -> ())) {
        Loader.show()
        let request = AF.request("http://api.androidhive.info/json/movies.json", method: .post)
        request.responseJSON { (response) in
            Loader.hide()
            print(response.value!)
            if let json = response.data {
                let decoder = JSONDecoder()
                do {
                    let response = try decoder.decode([MoviesList].self, from: json)
                    self.moviesList = response
                    handler()
                } catch let error  {
                    print("Parsing Failed \(error.localizedDescription)")
                }
            }
        }
        
    }

}


