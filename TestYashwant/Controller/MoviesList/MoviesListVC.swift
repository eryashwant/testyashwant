//
//  MoviesListVC.swift
//  TestYashwant
//
//  Created by whf on 20/05/20.
//  Copyright © 2020 Yashwant. All rights reserved.
//

import UIKit

class MoviesListVC: UIViewController {

    @IBOutlet weak var tblMovie: UITableView!
    
    // MARK: - Instance Properties
    var viewModel = MoviesListVM()
    
    // MARK: - Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
    }
    
    // MARK: - Private Methods
    private func initialSetup() {
        try? addReachabilityObserver()
        /*viewModel.callListingApi {
            DispatchQueue.main.async {
                self.tblMovie.reloadData()
            }
        }*/
    }

    private func configureTable() {
        tblMovie.estimatedRowHeight = 170
        tblMovie.rowHeight = UITableView.automaticDimension
    }
    func ShowAlert() {
        let alert = UIAlertController(title: "Alert", message: "No internet connection", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { _ in
            //try? self.addReachabilityObserver()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    deinit {
      removeReachabilityObserver()
    }
}

//MARK:- ReachabilityDelegate
extension MoviesListVC: ReachabilityObserverDelegate {
    func reachabilityChanged(_ isReachable: Bool) {
        if !isReachable {
            print("No internet connection")
            ShowAlert()
            //try? self.addReachabilityObserver()
        } else {
            viewModel.callListingApi {
                DispatchQueue.main.async {
                    self.tblMovie.reloadData()
                }
            }
        }
    }
}

//MARK:- UITableView DataSource

extension MoviesListVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.moviesList.count
        //return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblMovie.dequeueReusableCell(withIdentifier: "MoviesListCell") as! MoviesListCell
        let detail = self.viewModel.moviesList[indexPath.row]
        cell.configure(detail)
        return cell
    }
    
}

extension MoviesListVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
