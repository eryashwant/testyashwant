//
//  MoviesListCell.swift
//  TestYashwant
//
//  Created by whf on 20/05/20.
//  Copyright © 2020 Yashwant. All rights reserved.
//

import UIKit

class MoviesListCell: UITableViewCell {

    @IBOutlet weak var imgMovie: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblGenre: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(_ moviesList: MoviesList) {
        let imgUrl = moviesList.image
        imgMovie.setImageWithPlaceholder(with: URL(string: imgUrl), placeholderImage: #imageLiteral(resourceName: "ic_profile"))
        lblTitle.text = "Title:" + " " + moviesList.title
        let rating = moviesList.rating
        lblRating.text = "Rating:" + " " + "\(rating)"
        lblGenre.text = moviesList.genre.joined(separator: ", ")
    }
}
